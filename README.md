**PHP REST API KEY-VALUE STORE**

1. Accept a key-value pair and store them. If key exists, value should be updated
2. If key is provided, get value of key
3. Display all records of key-value pairs and data
4. For a given key and timestamp, return the value at that time. Timestamps are in unix

Below are the main endpoints:

- {BASE_URL}api/somekey [GET]
- {BASE_URL}api/somekey?timestamp=someTS [GET]
- {BASE_URL}api/get_all_records [GET]
- {BASE_URL}api [POST]

The PHP Rest-API is deployed on Ubuntu on Apache server and with data stored in MySQL database also stored on the instance.
Another instance with similar LAMP stack and Jenkins performs the testing and management of jobs where it pulls code from BitBucket.

For hosting, the servers are using Digital Ocean on cloud with a UFW firewall to manage ports, but this part is optional.