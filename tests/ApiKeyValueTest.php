<?php

use PHPUnit\Framework\TestCase;

class ApiKeyValueTest extends TestCase {

	protected $client;

	protected function setUp(): void
	{
		$this->client = new GuzzleHttp\Client([
			'base_uri' => 'http://128.199.207.131/secretlab_cicd/'
		]);
	}

	public function testGet_ValidInput_ApiObject()
	{
		$response = $this->client->request('GET', 'api/get_all_records', [
			// 'debug' => true
		]);

		$this->assertEquals(200, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];

		$this->assertRegexp("~application/json~", $contentType);

		$data = json_decode($response->getBody(), true);

		foreach ($data as $d) {
			$this->assertArrayHasKey('id_api', $d);
			$this->assertArrayHasKey('api_key', $d);
			$this->assertArrayHasKey('api_value', $d);
		}
    }

	public function testPost_NewApiObject()
	{
		$len1 = random_int(1, 11);
		$len2 = random_int(1, 11);

		$api_key = substr(uniqid(), 0, $len1);
		$api_val = substr(md5(time()), 0, $len2);

		$response = $this->client->request('POST', 'api', [
			'json' => [
				$api_key => $api_val
			]
		]);

		$this->assertEquals(200, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];

		$this->assertRegexp("~application/json~", $contentType);

		$data = json_decode($response->getBody(), true);

		$this->assertArrayHasKey('message', $data);

		$this->assertEquals('API key-value Created/Updated', $data['message']);
	}

	public function testGet_ApiObject()
	{
		$keyy = '6091'; # some key

		$get_uri = 'api/' . $keyy;

		$response = $this->client->request('GET', $get_uri, [
			// 'debug' => true
		]);

		$this->assertEquals(200, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];

		$this->assertRegexp("~application/json~", $contentType);

		$data = json_decode($response->getBody(), true);

		$this->assertArrayHasKey('api_value', $data);
	}

	public function testPost_UpdateValue()
	{
		$len1 = random_int(1, 5);

		$new_val = substr(uniqid(), 0, $len1);

		$api_key = '6'; # some key

		$response = $this->client->request('POST', 'api', [
			'json' => [
				$api_key => $new_val
			]
		]);

		$this->assertEquals(200, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];

		$this->assertRegexp("~application/json~", $contentType);

		$data = json_decode($response->getBody(), true);

		$this->assertArrayHasKey('message', $data);

		$this->assertEquals('API key-value Created/Updated', $data['message']);
	}

	public function testGet_NewValue_ApiObject()
	{
		$keyy = 'abc'; # some key
		$oldval = '123'; # '000' (FAIL)

		$get_uri = 'api/' . $keyy;

		$response = $this->client->request('GET', $get_uri, [
			// 'debug' => true
		]);

		$this->assertEquals(200, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];

		$this->assertRegexp("~application/json~", $contentType);

		$data = json_decode($response->getBody(), true);

		$this->assertArrayHasKey('api_value', $data);

		$this->assertEquals($oldval, $data['api_value']);
	}

	public function testGet_Timestamp_ApiObject()
	{
		$unix_time = (new DateTime('2021-05-05 01:30:00'))->getTimestamp();

		$unix_time = (string) $unix_time;

		$get_uri = 'api/609'; # some key

		$response = $this->client->request('GET', $get_uri, [
			'debug' => true,
			[
				'query' => [
					'timestamp' => $unix_time
				]
			]
		]);

		$this->assertEquals(200, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];

		$this->assertRegexp("~application/json~", $contentType);

		$data = json_decode($response->getBody(), true);

		$this->assertArrayHasKey('api_value', $data);

		$this->assertEquals('a', $data['api_value']);
	}

	public function testDelete_Error()
	{
		$response = $this->client->request('DELETE', 'api', [
			'http_errors' => false
		]);

	    $this->assertEquals(405, $response->getStatusCode());

		$contentType = $response->getHeaders()["Content-Type"][0];

		$this->assertRegexp("~application/json~", $contentType);

		$data = json_decode($response->getBody(), true);

		$this->assertArrayHasKey('error', $data);

		$this->assertEquals('Response not Found', $data['error']);
	}
}

?>