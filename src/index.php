<?php

include 'lib/core.load.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET,POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$reqMethod = $_SERVER["REQUEST_METHOD"];

// $unix_time = (new DateTime('2021-04-25 22:47:45'))->getTimestamp();
// echo $unix_time;

$controller = new DB_API_Log('api_log', $reqMethod);
$controller->processData();

?>
