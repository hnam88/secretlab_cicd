<?php
error_reporting(1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
// ini_set('memory_limit', '-1');
ini_set('display_errors', TRUE);

date_default_timezone_set("Asia/Singapore");

define('ABSPATH', dirname(dirname(__FILE__)).'/');

if (file_exists(ABSPATH . "lib/config.php"))
{
	include(ABSPATH . "lib/core.connection.php");
}
else
{
	die("<h1>Configuration file not exist</h1><p>There doesn't seem to be a <code>config</code> file in <code>lib</code>. I need this before we can get started. You can create a <code>config</code> file through a web interface, but this doesn't work for all server setups. The safest way is to manually create the file.</p>Configuration file not exist");
}

include(ABSPATH . "lib/core.db.php");

?>