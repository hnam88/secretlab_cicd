<?php
/**
 * Description: The DBObject class is the generic database object. It is not to be used directly, but extended
 * by additional classes, each corresponding to a database table. This particular DBObject is for use with a 
 * MySQL PDO driver.
 * Dependencies: A database connection script that uses the MySQL PDO extension
 */
class DB_API_Log extends Connection {

    protected $db;
    protected $table;
    protected $uri = array();
    protected $reqMethod;

    function __construct($table, $reqMethod) {
		$this->db = $this->getConnection();
        $this->table = $table;
        $this->reqMethod = $reqMethod;
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $this->uri = explode('/', $uri );
    }

    public function processData() {
        switch ($this->reqMethod) {
            case 'GET':
                $response = $this->validateEndpoint($this->reqMethod);
                
                if (!$response) {
                    $itemId = $this->uri[array_key_last($this->uri)];

                    if ($itemId != 'get_all_records') {
                        $response = $this->getItem($itemId);
                    }
                    else {
                        $response = $this->getAll();
                    }
                }
                break;
            case 'POST':
                $response = $this->validateEndpoint($this->reqMethod);

                if (!$response) {
                    $response = $this->createItem();
                }
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
    
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    public function getAll() {
        $query = "SELECT id_api, api_key, api_value, added_on FROM " . $this->table . ";";

        try {
            $result = $this->db->query($query);
            $result = $result->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    public function createItem() {
        $data = json_decode(file_get_contents("php://input"));

        if (is_null($data)) {
            return $this->unprocessableEntityResponse();
        }

        try {
            foreach($data as $k => $v){
                $exists = $this->find($k);

                if ($exists) {
                    // update
                    $query = 'UPDATE ' . $this->table . ' SET api_value = :api_value WHERE api_key = :api_key;';
                }
                else {
                    // insert
                    $query = 'INSERT INTO ' . $this->table . ' (api_key, api_value) VALUES (:api_key, :api_value);';
                }

                $result = $this->db->prepare($query);
                $result->execute(array(
                    'api_key' => $k,
                    'api_value'  => $v
                ));

                $result->rowCount();

                if ($exists) {
                    $ref_id = $exists['id_api'];
                }
                else {
                    $ref_id = $this->db->lastInsertId();
                }

                // log into api_log_data
                $query = 'INSERT INTO api_log_data (api_id, api_value) VALUES (:api_id, :api_val);';

                $result = $this->db->prepare($query);
                $result->execute(array(
                    'api_id' => $ref_id,
                    'api_val'  => $v
                ));

                $result->rowCount();
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        $this->getConnectionClose();

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(array('message' => 'API key-value Created/Updated'));
        return $response;
    }

    public function getItem($key) {

        $ts = isset($_GET['timestamp']) ? $_GET['timestamp'] : '';

        if ($ts !== '') {
            $dt = date("Y-m-d H:i:s", (int) $ts);

            $item = $this->find($key);

            if ($item) {
                $api_id = $item['id_api'];
                $result = $this->get_value($api_id, $dt);
            }
            else {
                return $this->notFoundResponse();
            }
        } else {
            $result = $this->find($key, 'single');
        }

        if (!$result) {
            return $this->notFoundResponse();
        }

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    public function find($key, $select='all') {
        if($select == 'all') {
            $fields = "id_api, api_key, api_value, added_on";
        }
        else {
            $fields = "api_value";
        }

        $query = "SELECT " . $fields . " FROM " . $this->table . " WHERE api_key = :api_key;";

        try {
            $result = $this->db->prepare($query);
            $result->execute(array('api_key' => $key));
            $result = $result->fetch(\PDO::FETCH_ASSOC);

            $this->getConnectionClose();
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function get_value($api_id, $dt, $table='api_log_data') {

        $query = "SELECT api_value FROM " . $table . " WHERE api_id = :api_id AND created_date <= :dt ORDER BY id_log_data DESC LIMIT 1;";

        try {
            $result = $this->db->prepare($query);
            $result->execute(array(
                'api_id' => $api_id,
                'dt' => $dt
            ));
            $result = $result->fetch(\PDO::FETCH_ASSOC);

            $this->getConnectionClose();
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    private function validateEndpoint($request) {

        if ($request == 'POST') {
            if ($this->uri[array_key_last($this->uri)] != 'api') {
                return $this->invalidEndpoint();
            }
        }
        else {
            // GET
            $pointer = array_search('api', $this->uri);

            $next = $this->uri[$pointer+1];
            $last = $this->uri[array_key_last($this->uri)];

            if (empty($next)) {
                return $this->invalidEndpoint();
            }
            elseif ($next == 'get_all_records' && $next != $last) {
                return $this->invalidEndpoint();
            }
        }

        return false;
    }

    // private function getRandomKey() {

    //     $pos = random_int(1, 11);

    //     $query = "SELECT api_key " . " FROM " . $this->table . " WHERE id_api = :id_api;";

    //     try {
    //         $result = $this->db->prepare($query);
    //         $result->execute(array('id_api' => $pos));
    //         $result = $result->fetch(\PDO::FETCH_ASSOC);

    //         $this->getConnectionClose();
    //         return $result;
    //     } catch (\PDOException $e) {
    //         exit($e->getMessage());
    //     }
    // }

    private function notFoundResponse() {
        
        $response['status_code_header'] = 'HTTP/1.1 405 Not Found';
        $response['body'] = json_encode([
            'error' => 'Response not Found'
        ]);

        return $response;
    }

    private function invalidEndpoint() {
        
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = json_encode([
            'error' => 'Invalid Endpoint'
        ]);

        return $response;
    }

    private function unprocessableEntityResponse() {

        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);

        return $response;
    }
}
?>