<?php
class Connection {
	/**
	* Stores the database connection object.
	* @access protected
	* @var database connection object
	*/
	protected $dbo = NULL;
	
	/**
	* Stores the class instance, created only once on invocation. 
	* Singleton object instance of the class DBWrapper
	* @access protected
	* @static
	*/
	protected static $instance = NULL;
	
	/**
	* When the constructor is called (which is called only once - singleton instance) 
	* the connection to the database is set.
	* @access protected
	*/ 
	protected function __construct()
	{
		$this->getConnection();
	}
	
	/**
	* Grabs the database settings from the config file
	* 
	* @access private
	*/
	private function loadConfig()
	{
		include_once(ABSPATH . "lib/config.php");
	}
	
	/**
	* Sets up the connection to the database.
	*/ 
	public function getConnection()
	{
		if(is_null($this->dbo))
		{
			list($dsn,$user, $password) = $this->setDSN();
			return $this->dbo = new PDO($dsn,$user,$password);
		}
	}
	
	/**
	* Constructs the database source name(dsn) after the config file is read.
	* 
	* @return array
	*/ 
	protected function setDSN()
	{
		$this->loadConfig();
		$dbtype = DB_TYPE;
		$dbname = DB_NAME;
		$location = DB_HOST;
		$user = DB_USER;
		$password = DB_PASSWORD;
		$dsn = $dbtype.":dbname=".$dbname.";host=".$location;
		return array($dsn, $user,$password);
	}
	
	/**
	* Constructs the database source name(dsn) after the config file is read.
	* 
	* @return array
	*/ 
	public function configVar()
	{
		$this->loadConfig();
		$prefix = DB_PREFIX;
		return array($prefix);
	}
	public function getConnectionClose()
	{
		$this->dbo = null;
		return $this->dbo;
	}
}
?>